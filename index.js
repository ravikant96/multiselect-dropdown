// To start our application set domcontent loaded eventlistner
if(typeof window != 'undefined') {
	window.addEventListener('DOMContentLoaded', async () => {
        
        // Instance of Page class created and stored in window.page variable for global access
        const page = window.page = new Page();
	});
}

// Page Class basically manages the application
class Page extends Map {

    constructor() {
        
        // To call the constructor of the Map class
        super();

        // Initializing the container variable and storing it in this
        this.container = document.querySelector('main');
        this.setup();
        this.addDefaultMultiSelect();
    }

    // Method to add default multiselect on page load.
    addDefaultMultiSelect() {

        const dataList = [
            'fever',
            'fev',
            'hey',
            'hello',
            'health',
            'money',
            'covid',
            'virus'
        ];
        const name = 'Default';

        const defaultMultiselect = new MultiSelect({dataList, name});
        this.set('default', defaultMultiselect);
        this.container.querySelector('.multiselects').appendChild(defaultMultiselect.container);
    }

    // Method includes seting up event listeners.
    setup() {

        this.container.querySelector('.add-multiselect').on('click', () => {
            this.container.querySelector('.add-container').classList.remove('hidden');
            this.container.querySelector('.add-multiselect').classList.add('hidden');
        });

        this.container.querySelector('.cancel').on('click', () => {
            this.container.querySelector('.add-container').classList.add('hidden');
            this.container.querySelector('.add-multiselect').classList.remove('hidden');
        });

        this.container.querySelector('.proceed').on('click', () => {
            this.addMultiselect();
            this.container.querySelector('.add-container').classList.add('hidden');
            this.container.querySelector('.add-multiselect').classList.remove('hidden');
        });
    }

    // Method to add multiselect to the list post click on add button
    addMultiselect() {

        let dataList = this.container.querySelector('.add-container textarea').textContent;
        const name = this.container.querySelector('.add-container input[name="name"]').value;

        dataList = dataList.trim().split(',');

        const newMultiselect = new MultiSelect({dataList, name});
        this.set(name, newMultiselect);
        this.container.querySelector('.multiselects').appendChild(newMultiselect.container);
    }
}

// A general class to workaroud with multiselect
/*
    Parameters required
        1. dataList(required param) = It is the set of the tags which is provided by the user and used for the slection from the multi select. 
        2. name = It is basically the name given to the multiselect. It act's as a heading to the multiselect
    
    How to start
        Step-1: Create a datalist
        step-2: Provide a name
        step-3: Invoke the constructor of the MultiSelect
                var multiselect = new MultiSelect({dataList: <datalist>, name: <name>})
        step-4: Paste the container from the above multislect reference;
                <target-container>.appendChild(multiselect.container);
*/
class MultiSelect {

    constructor({ dataList = [], name} = {}) {

        this.dataList = [];
        this.name = name;
        
        for(const item of dataList) {
            
            this.dataList.push(new Tag(item, this));
        }

        this.suggestionList = [];
        this.selectedList = new Set();
    }

    // Container for multiselect which is invoked and pasted to the dom and shows multiselect
    get container() {

        if(this.containerElement) {
            return this.containerElement;
        }

        const container = this.containerElement = document.createElement('div');
        container.classList.add('multiselect');

        container.innerHTML = `

            <h3>${this.name}</h3>

            <div class="saerch">
                <div class="tags"></div>
                <div class="search-text" contenteditable="true"></div>
            </div>
            <div class="datalist"></div>
        `;

        container.querySelector('.search-text').on('keydown', (e) => {

            if(e.keyCode == 13) {

                e.target.contentEditable = false;
                this.createNewTag(e.target.textContent);
                e.target.textContent = null;
                e.target.contentEditable = true;
            }
        });

        container.querySelector('.search-text').on('blur', (e) => {
            
            this.createNewTag(e.target.textContent);
            e.target.textContent = null;
        })

        container.querySelector('.search-text').on('keyup', (e) => {

            if(this.searchTimer) {
                clearTimeout(this.searchTimer);
            }

            this.searchTimer = setTimeout(() => {
                
                this.suggestionList = [];

                const value = e.target.textContent;

                for(const item of this.dataList) {

                    if(!item.newCreated && item.name.includes(value)) {
                        this.suggestionList.push(item);
                    }
                }
                
                this.renderOptions(this.suggestionList);
            }, 300);

        });

        this.render();

        return container;
    }
    
    // Creates a new tah from the value provided and also checks weather the tag was created or not.
    createNewTag(value) {

        if(!value) {
            return;
        }

        let alreadyExist = this.dataList.filter(x => !x.newCreated && x.name == value); 

        if(alreadyExist.length) {
            
            alreadyExist = alreadyExist[0];

            this.selectedList.add(alreadyExist);
        }
        else {

            const newTag = new Tag(value, this);
            newTag.newCreated = true;

            this.selectedList.add(newTag);
            this.dataList.push(newTag);
            this.container.querySelector('.tags').appendChild(newTag.selectedContainer);

            const rect = this.container.querySelector('.tags').getBoundingClientRect();

            if(rect.width >= 420) {
                this.container.querySelector('.tags').parentElement.style["flex-wrap"] = "wrap";
            }

            if(rect.width >= 500) {
                this.container.querySelector('.tags').style["flex-wrap"] = "wrap";
            }
        }

        this.renderOptions();
    }

    // Renders the container which includes datalist render and the suggested tag list render.
    render() {

        const listContainer = this.container.querySelector('.datalist');
        const selectedtagContainer = this.container.querySelector('.tags');
        
        selectedtagContainer.textContent = null;
        listContainer.textContent = null;

        for(const tag of this.dataList) {

            if(this.selectedList.has(tag) || tag.newCreated) {
                
                continue;
            }

            listContainer.appendChild(tag.container);
        }

        for(const item of this.selectedList) {

            selectedtagContainer.appendChild(item.selectedContainer);
        }

        const rect = selectedtagContainer.getBoundingClientRect();

        if(rect.width >= 400) {
            selectedtagContainer.parentElement.style["flex-wrap"] = "wrap";
        }

        if(rect.width >= 500) {
            selectedtagContainer.style["flex-wrap"] = "wrap";
        }
    }

    // Method which renders the datalist from the list provided.
    renderOptions(list) {

        const listContainer = this.container.querySelector('.datalist');
        listContainer.textContent = null;

        if(!list) {
            list = this.dataList;
        }

        for(const tag of list) {

            if(this.selectedList.has(tag) || tag.newCreated) {
                
                continue;
            }

            listContainer.appendChild(tag.container);
        }
    }
}

/*
    A generalized class with all the functional activites related to a sepecific tag.
    
    Parameters detail
        1. name = It is a required parameter which hold the name of the tag.
        2. multiselect = it holds the reference of the multiselect which invoked it.
*/
class Tag {

    constructor(item, multiSelect) {

        this.name = item,
        this.multiSelect = multiSelect;
    }

    // Container of a tag which includes the name of the tag and a eventlistener to add that tag to selected list.
    get container() {

        if(this.containerElement) {
            return this.containerElement;
        }

        const container = this.containerElement = document.createElement('span');
        container.classList.add('tag');

        container.innerHTML = this.name;

        container.on('click', () => {

            this.multiSelect.selectedList.add(this);
            this.multiSelect.container.querySelector('.search-text').textContent = '';
            container.remove();
            this.multiSelect.render();
        });

        return container;
    }

    // Once the tag is added to the selected list, this container gets append to selected list of containers.
    // This containers includes a remove container which removes itself from the selected list and a contenteditable div which allows itslelf to get edited.
    get selectedContainer() {

        if(this.selectedContainerElement) {
            return this.selectedContainerElement;
        }

        const container = this.selectedContainerElement = document.createElement('div');
        container.classList.add('selected-tag');

        container.innerHTML = `
            <span class="remove">X</span>
            <span class="name" contenteditable="true">${this.name}</span>
        `;

        container.querySelector('.remove').on('click', () => {
            this.multiSelect.selectedList.delete(this);
            container.remove();
            this.multiSelect.render();
        });

        container.querySelector('.name').on('keyup', (e) => {

            this.edtingStart = true;
        });

        container.querySelector('.name').on('keydown', (e) => {
            
            if (e.keyCode === 13) {
                e.target.contentEditable = false;
                this.checkTag(e);
                e.target.contentEditable = true;
            }
        });

        container.querySelector('.name').on('blur', (e) => {

            this.checkTag(e);
        });

        return container;
    }

    // Method which internally checks wheather the new tag is already present or not. If not present, it creates the new tag.
    checkTag(event) {

        const newValue = event.target.textContent;

        if(newValue == this.name) {
            return;
        }

        const newTag = new Tag(this.name, this.multiSelect)
        newTag.newCreated = this.newCreated;

        this.newCreated = true;

        this.multiSelect.dataList.push(newTag);

        this.name = newValue;

        this.multiSelect.renderOptions(this.multiSelect.dataList);
    }
}

/* 
    Added a new on function to Node prototype.
    which internlly call addeventlistener method
*/
if(typeof Node != 'undefined') {
	Node.prototype.on = window.on = function(name, fn) {
		this.addEventListener(name, fn);
	}
}